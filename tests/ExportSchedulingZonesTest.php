<?php
/**
 * Created by PhpStorm.
 * User: panda-dev
 * Date: 22.07.15
 * Time: 13:48
 */

class ExportSchedulingZonesTest extends TestCase {


    public function testMake() {

        $obj = new \VeeRoute\Distribution_api\Objects\ExportSchedulingZones($this->config);
        $resp = $obj->make();

        $this->assertTrue(isset($resp->schedulingZones->zones), "you can export zones from account");

    }
}
 