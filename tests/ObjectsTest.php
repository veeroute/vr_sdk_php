<?php
/**
 * Created by PhpStorm.
 * User: panda-dev
 * Date: 20.07.15
 * Time: 13:37
 */

class ObjectsTest extends TestCase {

    public function testSetDriver() {

        $performers = array('performersDetails'=>
            array('performers'=>
                array('performer'=>
                    array(
                        '@attributes'=>array(
                            'name'=>"Driver_1",
                            'phone'=>'79110002201',
                            'email'=>"driver_1@veeroute.com",
                            'login'=>"driver_1@veeroute.com",
                            'password'=>"driver_2_79110002201",
                            'priceForOneHour'=>10,
                            'breakShift'=>0,
                            'areaOfControl'=>'Центральное депо'
                        ),
                        'availability'=>array(
                            array(
                                '@attributes'=>array(
                                    'weekDay'=>"Monday",
                                    'from'=>'09:00',
                                    'to'=>'18:00'
                                )
                            ),
                            array(
                                '@attributes'=>array(
                                    'weekDay'=>"Tuesday",
                                    'from'=>'09:00',
                                    'to'=>'18:00'
                                )
                            )
                        )
                    )
                )
            )
        );


        $veeroute_connection = new \VeeRoute\Distribution_api\Objects\ImportPerformers($this->config);

        $result = $veeroute_connection->make($performers);

        $this->assertTrue(isset($result->performers), "we can create/update performers");

    }

    public function testSetVehicle() {
        $vehicles = array('vehiclesDetails'=>
            array('vehicles'=>
                array('vehicle'=>
                    array(
                        '@attributes'=>array(
                            'name'=>"Vehicle_1",
                            'active'=>true,
                            'costByDistance'=>10,
                            'maxWeight'=>20,
                            'averageSpeed'=>120,
                            'areaOfControl'=>'Центральное депо'
                        )
                    )
                )
            )
        );


        $veeroute_connection = new \VeeRoute\Distribution_api\Objects\ImportVehicles($this->config);

        $result = $veeroute_connection->make($vehicles);

        $this->assertTrue(isset($result->objects), "we can create/update vehicles");

    }


    public function testAssignPerformersToVehicles() {

        $assign = array('allocationsDetails'=>
            array('allocations'=>
                array('allocation'=>
                    array(
                        '@attributes'=>array(
                            'performerName'=>"Driver_1",
                            'vehicleName'=>"Vehicle_1",
                        )
                    )
                )
            )
        );

        $veeroute_connection = new \VeeRoute\Distribution_api\Objects\AssignPerformersToVehicles($this->config);

        $result = $veeroute_connection->make($assign);

        $this->assertTrue(isset($result->objects), "we can create/update assign performers to vehicles");

    }
} 