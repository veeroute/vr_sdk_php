<?php
/**
 * Created by PhpStorm.
 * User: panda-dev
 * Date: 22.07.15
 * Time: 18:16
 */

class GetAreaOfControlsTest extends TestCase {

    public function testMake() {

        $obj = new \VeeRoute\Distribution_api\Objects\GetAreaOfControls($this->config);

        $areaOfControls = $obj->make();

        $this->assertTrue(isset($areaOfControls->areaOfControlResponse->aocs), "we can get areaOfControls");

    }

}
 