# VeeRoute SDK
---
## Установка
### Через composer
`composer require veeroute/sdk_php:dev-master`
### Через git
* Cклонируйте проект `git clone git@bitbucket.org:veeroute/vr_sdk_php.git`
* Обновите зависимости при помощи composer: `composer update` (команда выполняется из папки `vr_sdk_php`)
### Скачать как архив
* Перейдите в раздел [Downloads](https://bitbucket.org/veeroute/vr_sdk_php/downloads), после чего скачайте последнюю версию репозитория. Ссылка - `Download repository`.
* Распакуйте архив в папке с вашим проектом
* Обновите зависимости при помощи composer: `composer update`


Подробнее об установке composer: https://getcomposer.org/doc/00-intro.md

## Структура
* TODO: описать то, что у каждого класса есть описанние
* TODO: описать принцип использования классов/их добавления

## Тесты
В папке с тестами (`tests`) вы можете найти примеры использования SDK. Перед использованием перейдите в файл `tests/TestCase.php` и заполните его на основании данных вашего аккаунта.

## Ссылки
* [Описание всех методов API](http://account.veeroute.com/knowledge/dev)