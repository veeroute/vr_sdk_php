<?php
/**
 * Created by PhpStorm.
 * User: panda-dev
 * Date: 22.07.15
 * Time: 13:43
 */
namespace VeeRoute\Builder\Url;

use VeeRoute\VeeRouteException;

/**
 * Class Auth
 * @package VeeRoute\Builder\Url
 * @description шаблон для запросов, когда требуется только авторизация через GET строку.
 */
class Auth extends \VeeRoute\Builder\Url {

    protected $auth_require = false;

    protected function getRequestParams($content) {

        if(!isset($this->account) || !isset($this->user) || !isset($this->password)) {
            throw new VeeRouteException('you have missed param for authentication: accountID or username or password');
        }

        $params = array(
            'accountID'=>$this->account,
            'user'=>$this->user,
            'password'=>$this->password
        );

        return $params;

    }

} 