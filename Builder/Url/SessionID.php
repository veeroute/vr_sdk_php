<?php
/**
 * Created by PhpStorm.
 * User: panda-dev
 * Date: 22.07.15
 * Time: 18:12
 */

namespace VeeRoute\Builder\Url;

use VeeRoute\VeeRouteException;

/**
 * Class SessionID
 * @package VeeRoute\Builder\Url
 * @description шаблон для запросов, которым нужен только sessionID
 */
class SessionID extends \VeeRoute\Builder\Url {

    protected function getRequestParams($content) {

        if(!isset($this->access_token)) {
            throw new VeeRouteException('We havent access token');
        }

        $params = array(
            'sessionID'=>$this->access_token
        );

        return $params;

    }
} 